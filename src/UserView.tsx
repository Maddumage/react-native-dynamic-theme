//import liraries
import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {AppContext} from './theme/AppContextProvider';

// create a component
const UserView = () => {
  const {theme, changeTheme} = React.useContext(AppContext);
  console.log(theme.colors.color);
  return (
    <View
      style={[
        styles.container,
        {backgroundColor: theme.colors.backgroundColor},
      ]}>
      <Text style={{color: theme.colors.color}}>Roshan Maddumage</Text>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});

//make this component available to the app
export default UserView;
