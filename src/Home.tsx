//import liraries
import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {AppContext} from './theme/AppContextProvider';
import {Dark, Light} from './theme/Themes';

// create a component
const Home = () => {
  const {theme, changeTheme} = React.useContext(AppContext);
  const {colors} = theme;
  console.log(colors);
  return (
    <View
      style={[
        styles.container,
        {backgroundColor: theme.colors.backgroundColor},
      ]}>
      <Text style={{color: theme.colors.color}}>Home</Text>
      <Button
        title={'Change Theme to Dark'}
        onPress={() => {
          changeTheme(Dark);
        }}
      />
      <View style={{height: 10}} />
      <Button
        title={'Change Theme to Light'}
        onPress={() => {
          changeTheme(Light);
        }}
      />
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});

//make this component available to the app
export default Home;
