import React from 'react';
import {AppContextProvider} from './theme/AppContextProvider';
import Home from './Home';
import UserView from './UserView';

const App: React.FC = () => {
  return (
    <AppContextProvider>
      <UserView />
      <Home />
    </AppContextProvider>
  );
};

export default App;
