import {DefaultTheme} from 'react-native-paper';

export const Dark = {
  ...DefaultTheme,
  dark: true,
  colors: {
    ...DefaultTheme.colors,
    color: '#aaa',
    backgroundColor: '#333834',
  },
};

export const Light = {
  ...DefaultTheme,
  dark: false,
  colors: {
    ...DefaultTheme.colors,
    color: '#004080',
    backgroundColor: '#b3d9ff',
  },
};
