import React, {useState} from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import {Dark, Light} from './Themes';

interface Theme {
  theme: any;
  changeTheme: any;
}

export const AppContext = React.createContext<Theme>({
  theme: Light,
  changeTheme: () => {},
});

export const AppContextProvider = ({children}: any) => {
  const [theme, changeTheme] = useState(Light);
  return (
    <AppContext.Provider value={{theme: theme, changeTheme}}>
      <PaperProvider theme={theme}>{children}</PaperProvider>
    </AppContext.Provider>
  );
};
